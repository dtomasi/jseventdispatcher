/**
 * Javascript EventDispatcher
 * inspired by Symfony2 EventDispatcher
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @licence MIT
 */

/**
 * Serving a separate namespace
 * @type {JsEventDispatcher|*|{}}
 */
var JsEventDispatcher = JsEventDispatcher || {};

/**
 * Directly create new EventDispatcher()
 * @returns {JsEventDispatcher.EventDispatcher}
 * @constructor
 */
var EventDispatcher = function(){
    return new JsEventDispatcher.EventDispatcher();
};

/**
 * Event is already existing, so we need to prefix
 * @returns {JsEventDispatcher.Event}
 * @constructor
 */
EventDispatcher.Event = function(name){
    return new JsEventDispatcher.Event(name);
};
/**
 * Event
 * @param name {String}
 * @constructor
 */
JsEventDispatcher.Event = function (name) {

    /**
     * Define Vars
     */
    var _name = name,
        _scope,
        _arguments = {},
        _propagationStopped = false
        ;

    /**
     * Get the Name of this Event
     * @returns {String}
     */
    this.getName = function () {
        return _name;
    };

    /**
     * Define a Scope
     * @param scope {Object|Function}
     */
    this.setScope = function (scope) {
        _scope = scope;
        return this;
    };

    /**
     * Get the Scope
     * @returns {Object|Function}
     */
    this.getScope = function () {
        return _scope;
    };

    /**
     * Set arguments as object
     * @param args {Object}
     */
    this.setArguments = function (args) {
        _arguments = args;
        return this;
    };

    /**
     * Get the arguments
     * @returns {object}
     */
    this.getArguments = function () {
        return _arguments;
    };

    /**
     * Get the arguments
     * @param key {String}
     * @returns {*}
     */
    this.getArgument = function (key) {

        if (_arguments.hasOwnProperty(key)) {
            return _arguments[key];
        }
        return false;
    };

    /**
     * Add a Argument
     * @param key {String}
     * @param value {*}
     */
    this.addArgument = function (key, value) {
        _arguments[key] = value;
        return this;
    };

    /**
     * Remove a Argument
     * @param key {String}
     */
    this.removeArgument = function (key) {

        if (_arguments.hasOwnProperty(key)) {
            delete _arguments[key];
        }
        return this;
    };

    /**
     * Stop Propagation
     */
    this.stopPropagation = function () {
        _propagationStopped = true;
        return this;
    };

    /**
     * Check if Propagation was stopped
     * @returns {boolean}
     */
    this.isPropagationStopped = function () {
        return _propagationStopped;
    };
};
/**
 * EventDispatcher
 * @constructor
 */
JsEventDispatcher.EventDispatcher = function () {

    /**
     * Holds the EventListeners
     * @type {Object}
     */
    var eventListeners = {};

    /**
     * Dispatch a Event
     * @param event {JsEventDispatcher.Event}
     */
    this.dispatch = function (event) {

        if (!event instanceof Event) {
            throw new Error('A instance of Event must be passed');
        }

        if (!this.hasListener(event.getName())) {
            return;
        }

        eventListeners[event.getName()].sort(function (a, b) {
            return b.priority - a.priority;
        });

        for (var i = 0; i < eventListeners[event.getName()].length; i++) {
            eventListeners[event.getName()][i].callback.apply(event.getScope(), [event, this]);
            eventListeners[event.getName()][i].called++;

            if (event.isPropagationStopped()) {
                break;
            }
        }
    };

    /**
     * Add a Listener
     * @param event {String}
     * @param callback {Function}
     * @param [priority] {Integer}
     */
    this.addListener = function (event, callback, priority) {

        // Default-Priority is 100
        if (typeof priority == 'undefined') {
            priority = 100;
        }

        // Create event-name if not exists
        if (!eventListeners.hasOwnProperty(event)) {
            eventListeners[event] = [];
        }

        // Add Listener
        eventListeners[event].push(
            {
                event: event,
                callback: callback,
                priority: priority,
                called: 0
            }
        );
    };

    /**
     * Add Listeners by array
     *
     * Example:
     *     var listeners = [
     *          {
     *              "event": 'test',
     *              "callback": callback,
     *              "priority": 200
     *          }
     *     ];
     *
     * @param listeners {Array}
     */
    this.addListeners = function (listeners) {

        if (listeners.length > 0) {
            for (var i = 0; i < listeners.length; i++) {
                listeners[i] = this.addListener(
                    listeners[i].event,
                    listeners[i].callback,
                    listeners[i].priority
                );
            }
        }
    };

    /**
     * Remove a Listener
     * @param name {String}
     * @param callback {Function}
     */
    this.removeListener = function (name, callback) {

        if (!this.hasListener(name)) {
            return;
        }

        var listeners = eventListeners[name];

        for (var i = 0; i < listeners.length; i++) {
            if (listeners[i].callback == callback) {
                eventListeners[name].splice(i, 1);
            }
        }
    };

    /**
     * Check if a Event has Listeners
     * @param name {String}
     */
    this.hasListener = function (name) {
        return eventListeners.hasOwnProperty(name);
    };

    /**
     * Get all Listeners
     * @returns {Object}
     */
    this.getListeners = function () {
        return eventListeners;
    };

    /**
     * Print Listeners to Console
     * @returns {string}
     */
    this.debug = function () {

        console.log('*********** JsEventDispatcher Debug ***********');
        console.log(eventListeners);
        console.log('***********************************************');
    };
};