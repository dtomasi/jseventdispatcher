/**
 * Javascript EventDispatcher
 * inspired by Symfony2 EventDispatcher
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @licence MIT
 */

QUnit.module("EventDispatcher Tests");
QUnit.test('create new Instance', function (assert) {

    var dispatcher = new JsEventDispatcher.EventDispatcher();
    assert.ok(
        dispatcher instanceof JsEventDispatcher.EventDispatcher,
        'dispatcher is a instance of JsEventDispatcher.EventDispatcher'
    );

});

QUnit.test('create new Instance by short Name', function (assert) {

    var dispatcher = new EventDispatcher();
    assert.ok(
        dispatcher instanceof JsEventDispatcher.EventDispatcher,
        'dispatcher is a instance of JsEventDispatcher.EventDispatcher'
    );

});

QUnit.test('getListeners', function (assert) {

    var dispatcher = new JsEventDispatcher.EventDispatcher();
    var listeners = dispatcher.getListeners();
    assert.ok(listeners instanceof Object, 'getListeners() returns a valid Object');

});

QUnit.test('addListener', function (assert) {

    var callback = function () {
        return true;
    };

    var dispatcher = new JsEventDispatcher.EventDispatcher();
    dispatcher.addListener('test', callback);

    var listeners = dispatcher.getListeners()['test'];
    var found = false;

    for (var i = 0; i < listeners.length; i++) {
        if (listeners[i].callback == callback) {
            found = true;
        }
    }
    assert.ok(found, 'Listener was successfully added');

});

QUnit.test('hasListener', function (assert) {

    var callback = function () {
        return true;
    };

    var dispatcher = new JsEventDispatcher.EventDispatcher();
    dispatcher.addListener('test', callback);

    assert.ok(dispatcher.hasListener('test'), 'dispatcher has listeners for event "test"');
});

QUnit.test('removeListeners', function (assert) {

    var callback = function () {
        return true;
    };

    var dispatcher = new JsEventDispatcher.EventDispatcher();
    dispatcher.addListener('test', callback);
    dispatcher.addListener('test', function () {
    });

    dispatcher.removeListener('test', callback);

    var listeners = dispatcher.getListeners()['test'];
    var found = false;

    for (var i = 0; i < listeners.length; i++) {
        if (listeners[i].callback == callback) {
            found = true;
        }
    }
    assert.ok(found == false, 'Added listener was successfully removed');

});

QUnit.test('addListeners', function (assert) {

    var callback = function () {
        return true;
    };

    var listeners = [
        {
            "event": 'test',
            "callback": callback,
            "priority": 200
        },
        {
            "event": 'test',
            "callback": callback,
            "priority": 10
        },
        {
            "event": 'test2',
            "callback": callback,
            "priority": 200
        }
    ];

    var dispatcher = new JsEventDispatcher.EventDispatcher();
    dispatcher.addListeners(listeners);

    assert.ok(dispatcher.hasListener('test'), 'Dispatcher has Listeners for event "test"');
    assert.ok(dispatcher.hasListener('test2'), 'Dispatcher has Listeners for event "test2"')

});


QUnit.test('dispatch', function (assert) {

    var callback = function (event) {
        event.getScope().ok(true, 'Callback was called and right Scope is provided');
    };

    var dispatcher = new JsEventDispatcher.EventDispatcher();

    dispatcher.addListener('myTestEvent', callback);

    var event = new JsEventDispatcher.Event('myTestEvent');
    event.setScope(assert);
    dispatcher.dispatch(event);

});

QUnit.test('dispatch by priority', function (assert) {

    var callback1 = function (event) {
        event.addArgument('called', true);
    };

    var callback2 = function (event) {
        event.getScope().equal(event.getArgument('called'), true, 'callback1 is called before callback2')
    };

    var dispatcher = new JsEventDispatcher.EventDispatcher();

    dispatcher.addListener('myTestEvent', callback1);
    dispatcher.addListener('myTestEvent', callback2, 99);

    var event = new JsEventDispatcher.Event('myTestEvent');
    event.setScope(assert);
    dispatcher.dispatch(event);

});

QUnit.test('dispatch serving instance of EventDispatcher', function (assert) {

    var callback = function (event,dispatcher) {
        this.ok(true, 'Callback was called and right Scope is provided');
        this.ok(dispatcher instanceof JsEventDispatcher.EventDispatcher);
    };

    var dispatcher = new JsEventDispatcher.EventDispatcher();

    dispatcher.addListener('myTestEvent', callback);

    var event = new JsEventDispatcher.Event('myTestEvent');
    event.setScope(assert);
    dispatcher.dispatch(event);

});

QUnit.test('dispatch is counting calls', function (assert) {

    var dispatcher = new JsEventDispatcher.EventDispatcher();

    dispatcher.addListener('myTestEvent', function(){});

    var listeners = dispatcher.getListeners();
    assert.equal(listeners['myTestEvent'][0].called, 0, 'Listener is never called');

    dispatcher.dispatch(new JsEventDispatcher.Event('myTestEvent'));

    listeners = dispatcher.getListeners();
    assert.equal(listeners['myTestEvent'][0].called, 1, 'Listener is called once');

    dispatcher.dispatch(new JsEventDispatcher.Event('myTestEvent'));

    listeners = dispatcher.getListeners();
    assert.equal(listeners['myTestEvent'][0].called, 2, 'Listener is called twice');

});