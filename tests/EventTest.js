/**
 * Javascript EventDispatcher
 * inspired by Symfony2 EventDispatcher
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @licence MIT
 */

QUnit.module('Event Tests');
QUnit.test('create new Instance', function (assert) {

    var event = new JsEventDispatcher.Event('myTestEventName');
    assert.ok(
        event instanceof JsEventDispatcher.Event,
        'event is a instance of JsEventDispatcher.Event'
    );

});

QUnit.test('create new Instance by short Name', function (assert) {

    var event = new EventDispatcher.Event('myTestEventName');
    assert.ok(
        event instanceof JsEventDispatcher.Event,
        'event is a instance of JsEventDispatcher.Event'
    );

});

QUnit.test('getName', function (assert) {

    var event = new JsEventDispatcher.Event('myTestEventName');
    assert.equal(event.getName(), 'myTestEventName');

});

QUnit.test('set and get Scope', function (assert) {

    var event = new JsEventDispatcher.Event('myTestEventName');

    var scope = new Object();
    event.setScope(scope);
    assert.equal(event.getScope(), scope, 'gotten scope is equal to set scope');

});

QUnit.test('set and get Arguments', function (assert) {

    var event = new JsEventDispatcher.Event('myTestEventName');

    event.setArguments({
        'myTestInteger': 200,
        'myTestString': 'string'
    });

    assert.equal(event.getArguments().myTestInteger, 200, 'gotten argument is equal to set argument');
    assert.equal(event.getArguments().myTestString, 'string', 'gotten argument is equal to set argument');

});

QUnit.test('add and remove Argument', function (assert) {

    var event = new JsEventDispatcher.Event('myTestEventName');

    event.setArguments({
        'myTestInteger': 200,
        'myTestString': 'string'
    });

    event.addArgument('myTestArray', 'someOtherString');

    assert.equal(event.getArgument('myTestArray'), 'someOtherString', 'gotten argument is equal to added argument');

    event.removeArgument('myTestArray');

    assert.equal(event.getArgument('myTestArray'), false, 'argument was successfully removed');

});


QUnit.test('stopPropagation() and isPropagationStopped()', function (assert) {

    var event = new JsEventDispatcher.Event('myTestEventName');

    assert.equal(event.isPropagationStopped(),false, 'Propagation is not stopped actually');

    event.stopPropagation();

    assert.equal(event.isPropagationStopped(),true, 'Propagation is now stopped');

});