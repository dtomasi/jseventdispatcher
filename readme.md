# JsEventDispatcher
[ ![Codeship Status for dtomasi/JsEventDispatcher](https://codeship.com/projects/04c61360-eb4f-0132-741d-4aa2be008ba7/status?branch=master)](https://codeship.com/projects/83367)

JsEventDispatcher is a EventDispatcher written in pure Javascript. It is inspired by Symfony2 EventDispatcher.

## How it works

#### Create a Instance of EventDispatcher
```javascript
var dispatcher = new JsEventDispatcher.EventDispatcher();
```

#### Add Listener
```javascript
// Add a single Listener
dispatcher.addListener('myEventName',function(event){
    // do something
});


// Add multiple Listeners
dispatcher.addListeners(
[
        {
            "event": 'test',
            "callback": callback,
            "priority": 200
        },
        {
            "event": 'test',
            "callback": callback,
            "priority": 10
        },
        {
            "event": 'test2',
            "callback": callback,
            "priority": 200
        }
    ]
);

```

#### Dispatch a Event
```javascript
dispatcher.dispatch(new JsEventDispatcher.Event('myEventName'));
```

#### Using Arguments
```javascript
var event = new JsEventDispatcher.Event('myEventName');

// Set Arguments
event.setArguments({'myArg':'myValue','myArg2':'myValue2'})

// Add a Argument
event.addArgument('myArg3','myValue3');

// Remove a Argument
event.removeArgument('myArg3');

// Get a single Argument
var arg2 = event.getArgument('myArg2');

// Get all Arguments
var args = event.getArguments();
```

#### Using Scope
```javascript

var callback = function(event){
    // current scope is Window
    // you can access via "this" or "event.getScope()"
};

dispatcher.addListener('myEventName', callback);

dispatcher.dispatch(
    new JsEventDispatcher.Event('myEventName')
    .setScope(Window)
);

```

#### Get all Listeners for Debugging
```javascript
console.log(dispatcher.getListeners());
```