/**
 * Event
 * @param name {String}
 * @constructor
 */
JsEventDispatcher.Event = function (name) {

    /**
     * Define Vars
     */
    var _name = name,
        _scope,
        _arguments = {},
        _propagationStopped = false
        ;

    /**
     * Get the Name of this Event
     * @returns {String}
     */
    this.getName = function () {
        return _name;
    };

    /**
     * Define a Scope
     * @param scope {Object|Function}
     */
    this.setScope = function (scope) {
        _scope = scope;
        return this;
    };

    /**
     * Get the Scope
     * @returns {Object|Function}
     */
    this.getScope = function () {
        return _scope;
    };

    /**
     * Set arguments as object
     * @param args {Object}
     */
    this.setArguments = function (args) {
        _arguments = args;
        return this;
    };

    /**
     * Get the arguments
     * @returns {object}
     */
    this.getArguments = function () {
        return _arguments;
    };

    /**
     * Get the arguments
     * @param key {String}
     * @returns {*}
     */
    this.getArgument = function (key) {

        if (_arguments.hasOwnProperty(key)) {
            return _arguments[key];
        }
        return false;
    };

    /**
     * Add a Argument
     * @param key {String}
     * @param value {*}
     */
    this.addArgument = function (key, value) {
        _arguments[key] = value;
        return this;
    };

    /**
     * Remove a Argument
     * @param key {String}
     */
    this.removeArgument = function (key) {

        if (_arguments.hasOwnProperty(key)) {
            delete _arguments[key];
        }
        return this;
    };

    /**
     * Stop Propagation
     */
    this.stopPropagation = function () {
        _propagationStopped = true;
        return this;
    };

    /**
     * Check if Propagation was stopped
     * @returns {boolean}
     */
    this.isPropagationStopped = function () {
        return _propagationStopped;
    };
};