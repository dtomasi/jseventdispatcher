/**
 * Javascript EventDispatcher
 * inspired by Symfony2 EventDispatcher
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @licence MIT
 */

/**
 * Serving a separate namespace
 * @type {JsEventDispatcher|*|{}}
 */
var JsEventDispatcher = JsEventDispatcher || {};

/**
 * Directly create new EventDispatcher()
 * @returns {JsEventDispatcher.EventDispatcher}
 * @constructor
 */
var EventDispatcher = function(){
    return new JsEventDispatcher.EventDispatcher();
};

/**
 * Event is already existing, so we need to prefix
 * @returns {JsEventDispatcher.Event}
 * @constructor
 */
EventDispatcher.Event = function(name){
    return new JsEventDispatcher.Event(name);
};