/**
 * EventDispatcher
 * @constructor
 */
JsEventDispatcher.EventDispatcher = function () {

    /**
     * Holds the EventListeners
     * @type {Object}
     */
    var eventListeners = {};

    /**
     * Dispatch a Event
     * @param event {JsEventDispatcher.Event}
     */
    this.dispatch = function (event) {

        if (!event instanceof Event) {
            throw new Error('A instance of Event must be passed');
        }

        if (!this.hasListener(event.getName())) {
            return;
        }

        eventListeners[event.getName()].sort(function (a, b) {
            return b.priority - a.priority;
        });

        for (var i = 0; i < eventListeners[event.getName()].length; i++) {
            eventListeners[event.getName()][i].callback.apply(event.getScope(), [event, this]);
            eventListeners[event.getName()][i].called++;

            if (event.isPropagationStopped()) {
                break;
            }
        }
    };

    /**
     * Add a Listener
     * @param event {String}
     * @param callback {Function}
     * @param [priority] {Integer}
     */
    this.addListener = function (event, callback, priority) {

        // Default-Priority is 100
        if (typeof priority == 'undefined') {
            priority = 100;
        }

        // Create event-name if not exists
        if (!eventListeners.hasOwnProperty(event)) {
            eventListeners[event] = [];
        }

        // Add Listener
        eventListeners[event].push(
            {
                event: event,
                callback: callback,
                priority: priority,
                called: 0
            }
        );
    };

    /**
     * Add Listeners by array
     *
     * Example:
     *     var listeners = [
     *          {
     *              "event": 'test',
     *              "callback": callback,
     *              "priority": 200
     *          }
     *     ];
     *
     * @param listeners {Array}
     */
    this.addListeners = function (listeners) {

        if (listeners.length > 0) {
            for (var i = 0; i < listeners.length; i++) {
                listeners[i] = this.addListener(
                    listeners[i].event,
                    listeners[i].callback,
                    listeners[i].priority
                );
            }
        }
    };

    /**
     * Remove a Listener
     * @param name {String}
     * @param callback {Function}
     */
    this.removeListener = function (name, callback) {

        if (!this.hasListener(name)) {
            return;
        }

        var listeners = eventListeners[name];

        for (var i = 0; i < listeners.length; i++) {
            if (listeners[i].callback == callback) {
                eventListeners[name].splice(i, 1);
            }
        }
    };

    /**
     * Check if a Event has Listeners
     * @param name {String}
     */
    this.hasListener = function (name) {
        return eventListeners.hasOwnProperty(name);
    };

    /**
     * Get all Listeners
     * @returns {Object}
     */
    this.getListeners = function () {
        return eventListeners;
    };

    /**
     * Print Listeners to Console
     * @returns {string}
     */
    this.debug = function () {

        console.log('*********** JsEventDispatcher Debug ***********');
        console.log(eventListeners);
        console.log('***********************************************');
    };
};